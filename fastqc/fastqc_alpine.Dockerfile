# FastQC app
# Image: registry.gitlab.com/roysomak4/genbio_containers/fastqc

FROM alpine:3.15

LABEL maintainer="Somak Roy<somak.roy@cchmc.org>" \
    function="Fastqc image"

ENV FASTQC_VER=0.11.9

RUN apk add --no-cache --virtual=build-deps \
  wget \
  ca-certificates \
  unzip \
  gzip && \
  # install perl to run the fastqc wrapper
  apk add --no-cache perl openjdk11-jre freetype fontconfig ttf-dejavu && \
  # download FASTQC archive
  wget --no-check-certificate https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v${FASTQC_VER}.zip -O fastqc.zip && \
  unzip fastqc.zip && \
  rm fastqc.zip && \
  mv FastQC/* /usr/local/bin/ && \
  chmod +x /usr/local/bin/fastqc && \
  # clean up install mess
  apk del build-deps && \
  rm -rf /var/cache/apk/*
